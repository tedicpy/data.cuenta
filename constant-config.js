'use strict';

exports = module.exports = {
  timeToShowForm: 30*60*60*100, // 30 minutos

  preguntas: [
    {
      idPregunta: 1,
      descripcion: '¿Alguna vez te han escrito, enviado audios o vídeos con los que te sentiste atacadx o insegurx?'
    },
    {
      idPregunta: 2,
      descripcion: '¿Tu pareja o alguien conocidx, compañerxs del trabajo o de la facultad compartieron cosas íntimas tuyas o de alguien más sin permiso?'
    },
    {
      idPregunta: 3,
      descripcion: '¿Alguna vez te has abstenido de publicar algo en tus redes por miedo a que te amenacen, te intimiden o te hagan sentir simplemente mal por tu forma de pensar?'
    },
    {
      idPregunta: 4,
      descripcion: '¿Alguna vez te pasó que trataste de entrar a tus cuentas y alguien (no sabemos quién) cambió las credenciales y perdiste el acceso a tu identidad digital?'
    },
    {
      idPregunta: 5,
      descripcion: '¿Alguna vez tu pareja te ha solicitado alguna contraseña tuya para tener acceso a tus dispositivos o cuentas?'
    },
    {
      idPregunta: 6,
      descripcion: '¿Alguna vez te han pedido una foto o prueba sobre lo que estás haciendo o donde estás? Por ejemplo: Tu pareja te pide una selfie para ver cómo andás o no te cree que estás dormidx en tu casa.'
    },
    {
      idPregunta: 7,
      descripcion: '¿Te has buscado en las redes y has encontrado a otra persona con tu misma identidad, tus mismas fotos, hasta una selfie con tu mascota o tu pareja?'
    },
    {
      idPregunta: 8,
      descripcion: '¿Se llegó a divulgar información manipulada o falsa sobre tu persona en las redes, como fotos, mensajes de texto, etc?'
    },
    {
      idPregunta: 9,
      descripcion: '¿Algún compañerx de trabajo, jefe, profesor o pariente, te manda textos, vídeos o audios con mensajes "subidos de tono"? Por ejemplo: "Qué linda pollera andás hoy", "Mirá cómo te queda esa blusa". Comentarios de ese estilo.'
    }
  ],

  generos: [
    {
      value: 'femenino',
      label: 'Femenino'
    },
    {
      value: 'masculino',
      label: 'Masculino'
    },
    {
      value: 'otro',
      label: 'Otro'
    }
  ],

  edades: {
    desde: 12,
    hasta: 76
  }
};
