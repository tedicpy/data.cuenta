module.exports = [
  {
    "departamento": "Alto Paraguay",
    "ciudad": "Bahía Negra"
  },
  {
    "departamento": "Alto Paraguay",
    "ciudad": "Carmelo Peralta"
  },
  {
    "departamento": "Alto Paraguay",
    "ciudad": "Fuerte Olimpo"
  },
  {
    "departamento": "Alto Paraguay",
    "ciudad": "Puerto Casado"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "Ciudad del Este"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "Doctor Juan León Mallorquín"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "Doctor Raúl Peña"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "Domingo Martínez de Irala"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "Hernandarias"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "Iruña"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "Itakyry"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "Juan E. O'Leary"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "Los Cedrales"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "Mbaracayú"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "Minga Guazú"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "Minga Porá"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "Naranjal"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "Ñacunday"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "Presidente Franco"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "San Alberto"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "San Cristóbal"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "Santa Fe del Paraná"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "Santa Rita"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "Santa Rosa del Monday"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "Tavapy"
  },
  {
    "departamento": "Alto Paraná",
    "ciudad": "Yguazú"
  },
  {
    "departamento": "Amambay",
    "ciudad": "Bella Vista"
  },
  {
    "departamento": "Amambay",
    "ciudad": "Capitán Bado"
  },
  {
    "departamento": "Amambay",
    "ciudad": "Karapaí"
  },
  {
    "departamento": "Amambay",
    "ciudad": "Pedro Juan Caballero"
  },
  {
    "departamento": "Amambay",
    "ciudad": "Zanja Pytã"
  },
  {
    "departamento": "Boquerón",
    "ciudad": "Filadelfia"
  },
  {
    "departamento": "Boquerón",
    "ciudad": "Loma Plata"
  },
  {
    "departamento": "Boquerón",
    "ciudad": "Mariscal José Félix Estigarribia"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "3 de Febrero"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "Caaguazú"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "Carayaó"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "Coronel Oviedo"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "Doctor Cecilio Báez"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "Doctor J. Eulogio Estigarribia"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "Doctor Juan Manuel Frutos"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "José Domingo Ocampos"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "La Pastora"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "Mariscal Francisco Solano Lopez"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "Nueva Londres"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "Nueva Toledo"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "R.I. 3 Corrales"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "Raúl Arsenio Oviedo"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "Repatriación"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "San Joaquín"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "San José de los Arroyos"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "Santa Rosa del Mbutuy"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "Simón Bolivar"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "Tembiaporá"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "Vaquería"
  },
  {
    "departamento": "Caaguazú",
    "ciudad": "Yhú"
  },
  {
    "departamento": "Caazapá",
    "ciudad": "3 de Mayo"
  },
  {
    "departamento": "Caazapá",
    "ciudad": "Abaí"
  },
  {
    "departamento": "Caazapá",
    "ciudad": "Buena Vista"
  },
  {
    "departamento": "Caazapá",
    "ciudad": "Caazapá"
  },
  {
    "departamento": "Caazapá",
    "ciudad": "Maciel"
  },
  {
    "departamento": "Caazapá",
    "ciudad": "Doctor Moisés S. Bertoni"
  },
  {
    "departamento": "Caazapá",
    "ciudad": "Yegros"
  },
  {
    "departamento": "Caazapá",
    "ciudad": "General Higinio Morinigo"
  },
  {
    "departamento": "Caazapá",
    "ciudad": "San Juan Nepomuceno"
  },
  {
    "departamento": "Caazapá",
    "ciudad": "Tavaí"
  },
  {
    "departamento": "Caazapá",
    "ciudad": "Yuty"
  },
  {
   "departamento": "Canindeyú",
   "ciudad": "Corpus Christi"
 },
 {
   "departamento": "Canindeyú",
   "ciudad": "Villa Curuguaty"
 },
 {
   "departamento": "Canindeyú",
   "ciudad": "Francisco Caballero Alvarez"
 },
 {
   "departamento": "Canindeyú",
   "ciudad": "Itanará"
 },
 {
   "departamento": "Canindeyú",
   "ciudad": "Katueté"
 },
 {
   "departamento": "Canindeyú",
   "ciudad": "La Paloma del Espíritu Santo"
 },
 {
   "departamento": "Canindeyú",
   "ciudad": "Maracaná"
 },
 {
   "departamento": "Canindeyú",
   "ciudad": "Nueva Esperanza"
 },
 {
   "departamento": "Canindeyú",
   "ciudad": "Salto del Guairá"
 },
 {
   "departamento": "Canindeyú",
   "ciudad": "Villa Ygatimí"
 },
 {
   "departamento": "Canindeyú",
   "ciudad": "Yasy Cañy"
 },
 {
   "departamento": "Canindeyú",
   "ciudad": "Yby Pytá"
 },
 {
   "departamento": "Canindeyú",
   "ciudad": "Ypejhú"
 },
 {
   "departamento": "Canindeyú",
   "ciudad": "Ybyrarobaná"
 },
 {
   "departamento": "Capital",
   "ciudad": "Asunción"
 },
 {
   "departamento": "Central",
   "ciudad": "Areguá"
 },
 {
   "departamento": "Central",
   "ciudad": "Capiatá"
 },
 {
   "departamento": "Central",
   "ciudad": "Fernando de la Mora"
 },
 {
   "departamento": "Central",
   "ciudad": "Guarambaré"
 },
 {
   "departamento": "Central",
   "ciudad": "Itá"
 },
 {
   "departamento": "Central",
   "ciudad": "Itauguá"
 },
 {
   "departamento": "Central",
   "ciudad": "J. Augusto Saldivar"
 },
 {
   "departamento": "Central",
   "ciudad": "Lambaré"
 },
 {
   "departamento": "Central",
   "ciudad": "Limpio"
 },
 {
   "departamento": "Central",
   "ciudad": "Luque"
 },
 {
   "departamento": "Central",
   "ciudad": "Mariano Roque Alonso"
 },
 {
   "departamento": "Central",
   "ciudad": "Nueva Italia"
 },
 {
   "departamento": "Central",
   "ciudad": "Ñemby"
 },
 {
   "departamento": "Central",
   "ciudad": "San Antonio"
 },
 {
   "departamento": "Central",
   "ciudad": "San Lorenzo"
 },
 {
   "departamento": "Central",
   "ciudad": "Villa Elisa"
 },
 {
   "departamento": "Central",
   "ciudad": "Villeta"
 },
 {
   "departamento": "Central",
   "ciudad": "Ypacaraí"
 },
 {
   "departamento": "Central",
   "ciudad": "Ypané"
 },
 {
   "departamento": "Concepción",
   "ciudad": "Arroyito"
 },
 {
   "departamento": "Concepción",
   "ciudad": "Azote'y"
 },
 {
   "departamento": "Concepción",
   "ciudad": "Belén"
 },
 {
   "departamento": "Concepción",
   "ciudad": "Concepción"
 },
 {
   "departamento": "Concepción",
   "ciudad": "Horqueta"
 },
 {
   "departamento": "Concepción",
   "ciudad": "Loreto"
 },
 {
   "departamento": "Concepción",
   "ciudad": "Paso Barreto"
 },
 {
   "departamento": "Concepción",
   "ciudad": "San Alfredo"
 },
 {
   "departamento": "Concepción",
   "ciudad": "San Lázaro"
 },
 {
   "departamento": "Concepción",
   "ciudad": "San Carlos del Apa"
 },
 {
   "departamento": "Concepción",
   "ciudad": "Sargento José Félix López"
 },
 {
   "departamento": "Concepción",
   "ciudad": "Yby Ya'u"
 },
 {
   "departamento": "Cordillera",
   "ciudad": "Altos"
 },
 {
   "departamento": "Cordillera",
   "ciudad": "Arroyos y Esteros"
 },
 {
   "departamento": "Cordillera",
   "ciudad": "Atyrá"
 },
 {
   "departamento": "Cordillera",
   "ciudad": "Caacupé"
 },
 {
   "departamento": "Cordillera",
   "ciudad": "Caraguatay"
 },
 {
   "departamento": "Cordillera",
   "ciudad": "Emboscada"
 },
 {
   "departamento": "Cordillera",
   "ciudad": "Eusebio Ayala"
 },
 {
   "departamento": "Cordillera",
   "ciudad": "Isla Pucú"
 },
 {
   "departamento": "Cordillera",
   "ciudad": "Itacurubí de la Cordillera"
 },
 {
   "departamento": "Cordillera",
   "ciudad": "Juan de Mena"
 },
 {
   "departamento": "Cordillera",
   "ciudad": "Loma Grande"
 },
 {
   "departamento": "Cordillera",
   "ciudad": "Mbocayaty del Yhaguy"
 },
 {
   "departamento": "Cordillera",
   "ciudad": "Nueva Colombia"
 },
 {
   "departamento": "Cordillera",
   "ciudad": "Piribebuy"
 },
 {
   "departamento": "Cordillera",
   "ciudad": "Primero De Marzo"
 },
 {
   "departamento": "Cordillera",
   "ciudad": "San Bernardino"
 },
 {
   "departamento": "Cordillera",
   "ciudad": "San José Obrero"
 },
 {
   "departamento": "Cordillera",
   "ciudad": "Santa Elena"
 },
 {
   "departamento": "Cordillera",
   "ciudad": "Tobatí"
 },
 {
   "departamento": "Cordillera",
   "ciudad": "Valenzuela"
 },
 {
   "departamento": "Guairá",
   "ciudad": "Coronel Martínez"
 },
 {
   "departamento": "Guairá",
   "ciudad": "Borja"
 },
 {
   "departamento": "Guairá",
   "ciudad": "Doctor Bottrell"
 },
 {
   "departamento": "Guairá",
   "ciudad": "Gral. Eugenio A. Garay"
 },
 {
   "departamento": "Guairá",
   "ciudad": "Félix Pérez Cardozo"
 },
 {
   "departamento": "Guairá",
   "ciudad": "Independencia"
 },
 {
   "departamento": "Guairá",
   "ciudad": "Itapé"
 },
 {
   "departamento": "Guairá",
   "ciudad": "Iturbe"
 },
 {
   "departamento": "Guairá",
   "ciudad": "José Fassardi"
 },
 {
   "departamento": "Guairá",
   "ciudad": "Capitán Mauricio José Troche"
 },
 {
   "departamento": "Guairá",
   "ciudad": "Mbocayaty"
 },
 {
   "departamento": "Guairá",
   "ciudad": "Natalicio Talavera"
 },
 {
   "departamento": "Guairá",
   "ciudad": "Ñumí"
 },
 {
   "departamento": "Guairá",
   "ciudad": "Paso Yobai"
 },
 {
   "departamento": "Guairá",
   "ciudad": "San Salvador"
 },
 {
   "departamento": "Guairá",
   "ciudad": "Tebicuary"
 },
 {
   "departamento": "Guairá",
   "ciudad": "Villarrica"
 },
 {
   "departamento": "Guairá",
   "ciudad": "Yataity"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Alto Verá"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Bella Vista"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Cambyretá"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Capitán Meza"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Capitán Miranda"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Carlos Antonio López"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Carmen del Paraná"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Coronel Bogado"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Edelira"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Encarnación"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Fram"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "General Delgado"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "General Artigas"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Hohenau"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Itapúa Poty"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Jesús"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "José Leandro Oviedo"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "La Paz"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Mayor Julio Dionisio Otaño"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Natalio"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Nueva Alborada"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Obligado"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Pirapó"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "San Cosme y Damián"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "San Juan del Paraná"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "San Pedro del Paraná"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "San Rafael del Paraná"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Tomás Romero Pereira"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Trinidad"
 },
 {
   "departamento": "Itapúa",
   "ciudad": "Yatytay"
 },
 {
   "departamento": "Misiones",
   "ciudad": "Ayolas"
 },
 {
   "departamento": "Misiones",
   "ciudad": "San Ignacio"
 },
 {
   "departamento": "Misiones",
   "ciudad": "San Juan Bautista de las misiones"
 },
 {
   "departamento": "Misiones",
   "ciudad": "San Miguel"
 },
 {
   "departamento": "Misiones",
   "ciudad": "San Patricio"
 },
 {
   "departamento": "Misiones",
   "ciudad": "Santa María"
 },
 {
   "departamento": "Misiones",
   "ciudad": "Santa Rosa"
 },
 {
   "departamento": "Misiones",
   "ciudad": "Santiago"
 },
 {
   "departamento": "Misiones",
   "ciudad": "Villa Florida"
 },
 {
   "departamento": "Misiones",
   "ciudad": "Yabebyry"
 },
 {
   "departamento": "Ñeembucú",
   "ciudad": "Alberdi"
 },
 {
   "departamento": "Ñeembucú",
   "ciudad": "Cerrito"
 },
 {
   "departamento": "Ñeembucú",
   "ciudad": "Desmochados"
 },
 {
   "departamento": "Ñeembucú",
   "ciudad": "Gral. José Eduvigis Díaz"
 },
 {
   "departamento": "Ñeembucú",
   "ciudad": "Guazú-cuá"
 },
 {
   "departamento": "Ñeembucú",
   "ciudad": "Humaitá"
 },
 {
   "departamento": "Ñeembucú",
   "ciudad": "Isla Umbú"
 },
 {
   "departamento": "Ñeembucú",
   "ciudad": "Laureles"
 },
 {
   "departamento": "Ñeembucú",
   "ciudad": "Mayor José Dejesús Martínez"
 },
 {
   "departamento": "Ñeembucú",
   "ciudad": "Paso de Patria"
 },
 {
   "departamento": "Ñeembucú",
   "ciudad": "Pilar"
 },
 {
   "departamento": "Ñeembucú",
   "ciudad": "San Juan Bautista de Ñeembucú"
 },
 {
   "departamento": "Ñeembucú",
   "ciudad": "Tacuaras"
 },
 {
   "departamento": "Ñeembucú",
   "ciudad": "Villa Franca"
 },
 {
   "departamento": "Ñeembucú",
   "ciudad": "Villa Oliva"
 },
 {
   "departamento": "Ñeembucú",
   "ciudad": "Villalbín"
 },
 {
   "departamento": "Paraguarí",
   "ciudad": "Acahay"
 },
 {
   "departamento": "Paraguarí",
   "ciudad": "Caapucú"
 },
 {
   "departamento": "Paraguarí",
   "ciudad": "Carapeguá"
 },
 {
   "departamento": "Paraguarí",
   "ciudad": "Escobar"
 },
 {
   "departamento": "Paraguarí",
   "ciudad": "General Bernardino Caballero"
 },
 {
   "departamento": "Paraguarí",
   "ciudad": "La Colmena"
 },
 {
   "departamento": "Paraguarí",
   "ciudad": "María Antonia"
 },
 {
   "departamento": "Paraguarí",
   "ciudad": "Mbuyapey"
 },
 {
   "departamento": "Paraguarí",
   "ciudad": "Paraguarí"
 },
 {
   "departamento": "Paraguarí",
   "ciudad": "Pirayú"
 },
 {
   "departamento": "Paraguarí",
   "ciudad": "Quiindy"
 },
 {
   "departamento": "Paraguarí",
   "ciudad": "Quyquyhó"
 },
 {
   "departamento": "Paraguarí",
   "ciudad": "Roque Gonzalez de Santa Cruz"
 },
 {
   "departamento": "Paraguarí",
   "ciudad": "Sapucái"
 },
 {
   "departamento": "Paraguarí",
   "ciudad": "Tebicuary-mí"
 },
 {
   "departamento": "Paraguarí",
   "ciudad": "Yaguarón"
 },
 {
   "departamento": "Paraguarí",
   "ciudad": "Ybycuí"
 },
 {
   "departamento": "Paraguarí",
   "ciudad": "Ybytymí"
 },
 {
   "departamento": "Presidente Hayes",
   "ciudad": "Benjamín Aceval"
 },
 {
   "departamento": "Presidente Hayes",
   "ciudad": "General José María Bruguez"
 },
 {
   "departamento": "Presidente Hayes",
   "ciudad": "José Falcón"
 },
 {
   "departamento": "Presidente Hayes",
   "ciudad": "Nanawa"
 },
 {
   "departamento": "Presidente Hayes",
   "ciudad": "Puerto Pinasco"
 },
 {
   "departamento": "Presidente Hayes",
   "ciudad": "Tte. 1° Manuel Irala Fernández"
 },
 {
   "departamento": "Presidente Hayes",
   "ciudad": "Teniente Esteban Martínez"
 },
 {
   "departamento": "Presidente Hayes",
   "ciudad": "Villa Hayes"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "25 de Diciembre"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "Antequera"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "Capiibary"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "Choré"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "General Elizardo Aquino"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "General Francisco Isidoro Resquín"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "Guajayvi"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "Itacurubí del Rosario"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "Liberación"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "Lima"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "Nueva Germania"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "San Estanislao"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "San Pablo"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "San Pedro del Ycuamandyyú"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "San Vicente Pancholo"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "Santa Rosa del Aguaray"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "Tacuatí"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "Unión"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "Villa del Rosario"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "Yataity del Norte"
 },
 {
   "departamento": "San Pedro",
   "ciudad": "Yrybucua"
 }
];
