#!/bin/bash -e

# npm set progress=false

npm install
npm install -g gulp@3.9.0
# npm link gulp

# gulp build

# Esto se hace para que apunte al contenedor de mongodb y no al localhost
# sed -i 's/localhost/db/' server/config/environment/development.js

exec "$@"
