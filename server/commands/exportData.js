'use strict';

import { parse, Transform, AsyncParser } from 'json2csv';
import path from 'path';
import _ from 'lodash';
import { writeFileSync, createWriteStream, mkdirSync } from 'fs';
import Resultado from '../api/resultado/resultado.model';
import Encuesta from '../api/encuesta/encuesta.model';
import appConfig from '../../constant-config';

/**
 * RESULTADOS.csv (1 sólo registro)
 * ------------------------------
 * CAT 1,    CAT 2,   CAT 3
 * 2.658,   2.1478,   1.569
 * 
 * 
 * ENCUESTA.csv (varios registros)
 * ------------------------------
 * FECHA,   CAT 1,    CAT 2,   CAT 3,  PREGUNTA-1, PREGUNTA-2, ..., PREGUNTA-X, GENERO, DEPARTAMENTO, CIUDAD, EDAD
 * 15-07-2019 19:30,    2.658,   2.1478,    1.569,  SI, NO, NOSE, ...
 * 
 * Archivar los resultados por fecha de exportación
 * > Al ejecutar el script, generar una carpeta con la fecha del momento de ejecución
 * > y guardar dentro los archivos csv
 */
export function exportData (cb) {
  let date = new Date();
  date = date.getFullYear() + ("0" + (date.getMonth() + 1)).slice(-2) + ("0" + date.getDate()).slice(-2) + "_" + ("0" + date.getHours()).slice(-2) + "-" + ("0" + date.getMinutes()).slice(-2) + "-" + ("0" + date.getSeconds()).slice(-2)
  let prefixPath = `./exported-data`;
  let folderPath = `${prefixPath}/${date}`;
  let opt = { date, folderPath };

  mkdir(prefixPath);
  mkdir(folderPath);

  exportResultadosToCSV(opt, () => {
    console.log('ResultadosCollection exported to CSV');
    exportEncuestaToCSV(opt, () => {
      console.log('EncuestaCollection exported to CSV');
      console.log('Done');
      cb();
    });
  });
}


export function exportResultadosToCSV(opt, cb) {
  Resultado.findOne((err, resultado) => {
    let obj = {};
    obj.cat1 = Math.round(resultado.tRespSiCat1 / resultado.tRespCat1 * 100);
    obj.cat2 = Math.round(resultado.tRespSiCat2 / resultado.tRespCat2 * 100);
    obj.cat3 = Math.round(resultado.tRespSiCat3 / resultado.tRespCat3 * 100);
    _.assign(obj, _.omit(resultado.toObject(), ['_id', '__v']));
    let csv = parse(obj, {
      delimiter: ';'
    });
    writeFileSync(path.resolve(opt.folderPath, 'resultados.csv'), csv);
    cb();
  });
}


export function exportEncuestaToCSV(opt, cb) {
  const docStream = Encuesta.find().cursor({
    transform: (doc) => {
      doc = doc.toObject();
      let newPreguntas = {};
      doc.preguntas.forEach(preg => {
        newPreguntas[preg.idPregunta] = preg.respuesta;
      });
      doc.preguntas = newPreguntas;
      Reflect.deleteProperty(doc, '__v');
      return doc;
    }
  });
  let fields = ['createDate'];
  _.range(1, appConfig.preguntas.length+1).forEach(index => {
    fields.push(`preguntas.${index}`);
  });
  fields = _.union(fields, [
    'result.tRespCat1', 'result.tRespCat2', 'result.tRespCat3', 'result.tRespSiCat1', 'result.tRespSiCat2', 'result.tRespSiCat3',
    'genero', 'departamento', 'ciudad', 'edad'
  ]);
  const writeStream = createWriteStream(path.resolve(opt.folderPath, 'encuesta.csv'), { encoding: 'ascii' });
  const json2csv = new Transform({
    flatten: true,
    delimiter: ';',
    encoding: 'ascii',
    fields
  }, {
    objectMode: true,
    encoding: 'ascii'
  });

  docStream
    .pipe(json2csv)
    .pipe(writeStream)
    .on('finish', cb);
}


function mkdir(folderPath) {
  try {
    mkdirSync(path.resolve(folderPath));
  }
  catch(err) {
    if (err.code === 'EEXIST') {
      return;
    }
    console.error(err);
  }
}