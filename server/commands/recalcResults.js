'use strict';

import Resultado from '../api/resultado/resultado.model';
import Encuesta from '../api/encuesta/encuesta.model';
import { updateResults } from '../api/encuesta/encuesta.controller';
import { documentStream } from '../components/utils';


export function recalcResults (cb) {
  console.log('Recalc resulsts');
  Resultado.remove().then(() => {
    iterateEncuestas()
    .then(qty => {
      console.log(`${qty} encuestas procesadas`);
      console.log('Done');
    })
    .then(cb);
  });
}


function iterateEncuestas () {
  var qty = 0;
  return documentStream(Encuesta.find({}), encuesta => {
    qty++;
    // total de respuestas por categoría
    encuesta.result = {};
    encuesta.result.tRespCat1 = 0;
    encuesta.result.tRespCat2 = 0;
    encuesta.result.tRespCat3 = 0;
    // total de respuestas positivas por categoría
    encuesta.result.tRespSiCat1 = 0;
    encuesta.result.tRespSiCat2 = 0;
    encuesta.result.tRespSiCat3 = 0;

    encuesta.preguntas.forEach(pregunta => {
      var i = pregunta.idPregunta;
      //Acoso
      if (i == 1 || i == 2 || i == 5 || i == 6 || i == 7 || i == 8 || i == 9) {
        if (pregunta.respuesta == 'SI') {
          encuesta.result.tRespSiCat1++;
        }
        if (pregunta.respuesta == 'SI' || pregunta.respuesta == 'NO') {
          encuesta.result.tRespCat1++;
        }
      }
      //Discurso de odio
      if (i == 3 || i == 8) {
        if (pregunta.respuesta == 'SI') {
          encuesta.result.tRespSiCat2++;
        }
        if (pregunta.respuesta == 'SI' || pregunta.respuesta == 'NO') {
          encuesta.result.tRespCat2++;
        }
      }
      //Vigilancia
      if (i == 4 || i == 5 || i == 6) {
        if (pregunta.respuesta == 'SI') {
          encuesta.result.tRespSiCat3++;
        }
        if (pregunta.respuesta == 'SI' || pregunta.respuesta == 'NO') {
          encuesta.result.tRespCat3++;
        }
      }
    });

    return encuesta.save()
      .then(updateResults(encuesta.result));
  })
  .then(() => {
    return qty;
  });
}