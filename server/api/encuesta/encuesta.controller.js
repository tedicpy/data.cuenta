/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /api/encuestas              ->  index
 * POST    /api/encuestas              ->  create
 * GET     /api/encuestas/:id          ->  show
 * PUT     /api/encuestas/:id          ->  upsert
 * PATCH   /api/encuestas/:id          ->  patch
 * DELETE  /api/encuestas/:id          ->  destroy
 */

'use strict';

import https from 'https';
import querystring from 'querystring';
import jsonpatch from 'fast-json-patch';
import Encuesta from './encuesta.model';
import Resultado from './../resultado/resultado.model';
import appConfig from '../../../constant-config';

function respondWithResult(res, statusCode) {
  statusCode = statusCode || 200;
  return function (entity) {
    if (entity) {
      return res.status(statusCode).json(entity);
    }
    return null;
  };
}

function respond(res, statusCode) {
  statusCode = statusCode || 200;
  return function () {
    return res.status(statusCode).json();
  };
}

function patchUpdates(patches) {
  return function (entity) {
    try {
      // eslint-disable-next-line prefer-reflect
      jsonpatch.apply(entity, patches, /*validate*/ true);
    } catch (err) {
      return Promise.reject(err);
    }

    return entity.save();
  };
}

function removeEntity(res) {
  return function (entity) {
    if (entity) {
      return entity.remove()
        .then(() => {
          res.status(204).end();
        });
    }
  };
}

function handleEntityNotFound(res) {
  return function (entity) {
    if (!entity) {
      res.status(404).end();
      return null;
    }
    return entity;
  };
}

function handleError(res, statusCode) {
  statusCode = statusCode || 500;
  return function (err) {
    console.error(err);
    res.status(statusCode).send(err);
  };
}

// Gets a list of Encuestas
export function index(req, res) {
  return Encuesta.find().exec()
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Retorna el documento de una encuesta 
// durante un tiempo establecido desde el momento
// de en que se registraron los resultados de la encuesta
export function show(req, res) {
  return Encuesta.findById(req.params.id).exec()
    .then(encuesta => {
      if(!encuesta) {
        return res.status(404).send();
      }
      let maxTime = encuesta.createDate.getTime() + appConfig.timeToShowForm;
      let dateNow = Date.now();
      if (dateNow > maxTime) {
        return res.status(404).send();
      }
      return res.status(200).json(encuesta);
    })
    .catch(handleError(res));
}

// Checks if the user is not a robot
export function captcha(req, res) {
  const token = req.body.token;
  callRecapGoogle(token, function (isARobot) {
    return res.json(isARobot);
  });
}

//Calls google recaptcha service
export function callRecapGoogle(token, callback) {
  const serverKey = '6LcbcKoUAAAAAJw685eyZSVNK6wNCdDft9UOjGQ5';

  const postData = querystring.stringify({
    response: token,
    secret: serverKey
  });

  const options = {
    hostname: 'www.google.com',
    path: '/recaptcha/api/siteverify',
    method: 'POST',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
      'Content-Length': postData.length
    }
  };

  const req = https.request(options, (res) => {
    let data = '';

    res.on('data', (chunk) => {
      data += chunk;
    });

    res.on('end', () => {
      try {
        const parsedData = JSON.parse(data);
        const score = parsedData.score;
        if (score > 0.5) {
          callback(false);
        } else {
          callback(true);
        }
      } catch (error) {
        console.error('Error parsing response:', error);
        callback(true);
      }
    });
  });

  req.on('error', (error) => {
    console.error('Request error:', error);
    callback(true);
  });

  req.write(postData);
  req.end();
}

// Creates a new Encuesta in the DB
export function create(req, res) {
  let encuesta = new Encuesta(req.body);
  return encuesta.save()
  .then(updateResults(req.body.result))
  .then(() => {
    return res.status(200).json({_id: encuesta.id});
  })
  .catch(handleError(res));
}

//Calculate general result
export function updateResults(encuestaResult) {
  return function() {
    return Resultado.update({}, {
      $inc: {
        // total de personas encuestadas
        tPersonas: 1,
        // total de respuestas por categoría
        tRespCat1: encuestaResult.tRespCat1,
        tRespCat2: encuestaResult.tRespCat2,
        tRespCat3: encuestaResult.tRespCat3,
        // total de respuestas positivas por categoría
        tRespSiCat1: encuestaResult.tRespSiCat1,
        tRespSiCat2: encuestaResult.tRespSiCat2,
        tRespSiCat3: encuestaResult.tRespSiCat3
      }
    }, { upsert: true });
  }
}

// Update the given Encuesta in the DB at the specified ID
export function update(req, res) {
  let { input, value } = req.body;
  
  if (
    (input === 'genero' && !value.value)
    || (input === 'localidad' && (!value.departamento || !value.ciudad))
    || (input === 'edad' && !value)) {
    return res.status(400).json(new Error('faltan datos'));
  }

  let modifier = {$set:{}};
  if (input === 'genero') {
    modifier.$set.genero = value.value;
  }
  if (input === 'localidad') {
    modifier.$set.departamento = value.departamento;
    modifier.$set.ciudad = value.ciudad;
  }
  if (input === 'edad') {
    modifier.$set.edad = value;
  }
  
  return Encuesta.findOneAndUpdate({ _id: req.params.id }, modifier, { new: true }).exec()
    .then(encuestaDoc => {
      if (!encuestaDoc) {
        return res.status(404).json(new Error('no se encuentra la encuesta'));
      }
      res.status(201).send();
    })
    .catch(handleError(res));
}

// Updates an existing Encuesta in the DB
export function patch(req, res) {
  if (req.body._id) {
    Reflect.deleteProperty(req.body, '_id');
  }
  return Encuesta.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(patchUpdates(req.body))
    .then(respondWithResult(res))
    .catch(handleError(res));
}

// Deletes a Encuesta from the DB
export function destroy(req, res) {
  return Encuesta.findById(req.params.id).exec()
    .then(handleEntityNotFound(res))
    .then(removeEntity(res))
    .catch(handleError(res));
}
