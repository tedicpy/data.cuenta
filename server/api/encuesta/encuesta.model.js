'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './encuesta.events';

var EncuestaSchema = new mongoose.Schema({
  createDate: {
    type: Date,
    default: Date.now,
  },
  pais: String,
  testimonio: String,
  preguntas: [{
    idPregunta: Number,
    respuesta: String
  }],
  result: {
    // total de respuestas por categoría
    tRespCat1: Number,
    tRespCat2: Number,
    tRespCat3: Number,
    // total de respuestas positivas por categoría
    tRespSiCat1: Number,
    tRespSiCat2: Number,
    tRespSiCat3: Number
  },
  genero: String,
  departamento: String,
  ciudad: String,
  edad: Number
});

registerEvents(EncuestaSchema);
export default mongoose.model('Encuesta', EncuestaSchema);
