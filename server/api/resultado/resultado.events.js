/**
 * Resultado model events
 */

'use strict';

import {EventEmitter} from 'events';
var ResultadoEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
ResultadoEvents.setMaxListeners(0);

// Model events
var events = {
  save: 'save',
  remove: 'remove'
};

// Register the event emitter to the model events
function registerEvents(Resultado) {
  for(var e in events) {
    let event = events[e];
    Resultado.post(e, emitEvent(event));
  }
}

function emitEvent(event) {
  return function(doc) {
    ResultadoEvents.emit(event + ':' + doc._id, doc);
    ResultadoEvents.emit(event, doc);
  };
}

export {registerEvents};
export default ResultadoEvents;
