'use strict';

/* globals describe, expect, it, beforeEach, afterEach */

var app = require('../..');
import request from 'supertest';

var newResultado;

describe('Resultado API:', function() {
  describe('GET /api/resultados', function() {
    var resultados;

    beforeEach(function(done) {
      request(app)
        .get('/api/resultados')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          resultados = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      resultados.should.be.instanceOf(Array);
    });
  });

  describe('POST /api/resultados', function() {
    beforeEach(function(done) {
      request(app)
        .post('/api/resultados')
        .send({
          name: 'New Resultado',
          info: 'This is the brand new resultado!!!'
        })
        .expect(201)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          newResultado = res.body;
          done();
        });
    });

    it('should respond with the newly created resultado', function() {
      newResultado.name.should.equal('New Resultado');
      newResultado.info.should.equal('This is the brand new resultado!!!');
    });
  });

  describe('GET /api/resultados/:id', function() {
    var resultado;

    beforeEach(function(done) {
      request(app)
        .get(`/api/resultados/${newResultado._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          resultado = res.body;
          done();
        });
    });

    afterEach(function() {
      resultado = {};
    });

    it('should respond with the requested resultado', function() {
      resultado.name.should.equal('New Resultado');
      resultado.info.should.equal('This is the brand new resultado!!!');
    });
  });

  describe('PUT /api/resultados/:id', function() {
    var updatedResultado;

    beforeEach(function(done) {
      request(app)
        .put(`/api/resultados/${newResultado._id}`)
        .send({
          name: 'Updated Resultado',
          info: 'This is the updated resultado!!!'
        })
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          updatedResultado = res.body;
          done();
        });
    });

    afterEach(function() {
      updatedResultado = {};
    });

    it('should respond with the updated resultado', function() {
      updatedResultado.name.should.equal('Updated Resultado');
      updatedResultado.info.should.equal('This is the updated resultado!!!');
    });

    it('should respond with the updated resultado on a subsequent GET', function(done) {
      request(app)
        .get(`/api/resultados/${newResultado._id}`)
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if(err) {
            return done(err);
          }
          let resultado = res.body;

          resultado.name.should.equal('Updated Resultado');
          resultado.info.should.equal('This is the updated resultado!!!');

          done();
        });
    });
  });

  describe('PATCH /api/resultados/:id', function() {
    var patchedResultado;

    beforeEach(function(done) {
      request(app)
        .patch(`/api/resultados/${newResultado._id}`)
        .send([
          { op: 'replace', path: '/name', value: 'Patched Resultado' },
          { op: 'replace', path: '/info', value: 'This is the patched resultado!!!' }
        ])
        .expect(200)
        .expect('Content-Type', /json/)
        .end(function(err, res) {
          if(err) {
            return done(err);
          }
          patchedResultado = res.body;
          done();
        });
    });

    afterEach(function() {
      patchedResultado = {};
    });

    it('should respond with the patched resultado', function() {
      patchedResultado.name.should.equal('Patched Resultado');
      patchedResultado.info.should.equal('This is the patched resultado!!!');
    });
  });

  describe('DELETE /api/resultados/:id', function() {
    it('should respond with 204 on successful removal', function(done) {
      request(app)
        .delete(`/api/resultados/${newResultado._id}`)
        .expect(204)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });

    it('should respond with 404 when resultado does not exist', function(done) {
      request(app)
        .delete(`/api/resultados/${newResultado._id}`)
        .expect(404)
        .end(err => {
          if(err) {
            return done(err);
          }
          done();
        });
    });
  });
});
