'use strict';

import mongoose from 'mongoose';
import {registerEvents} from './resultado.events';

var ResultadoSchema = new mongoose.Schema({
  // total de personas encuestadas
  tPersonas: Number,
  // total de respuestas por categoría
  tRespCat1: Number,
  tRespCat2: Number,
  tRespCat3: Number,
  // total de respuestas positivas por categoría
  tRespSiCat1: Number,
  tRespSiCat2: Number,
  tRespSiCat3: Number
});

registerEvents(ResultadoSchema);
export default mongoose.model('Resultado', ResultadoSchema);