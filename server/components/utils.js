'use strict';

/**
 * https://mongoosejs.com/docs/api.html#query_Query-cursor
 */
export function documentStream(query, process) {
  let cursor = query.cursor();
  return iterate();

  function iterate() {
    return cursor.next().then(data => {
      if(!data) {
        return;
      }
      let result = process(data);
      if(result && result.then) {
        return result.then(iterate);
      }
      return iterate();
    });
  }
}
