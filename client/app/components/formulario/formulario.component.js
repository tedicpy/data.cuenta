'use strict';
import angular from 'angular';


export class formularioComponent {
  constructor($window, $http, appConfig, localidadesList) {
    'ngInject';
    this.$window = $window;
    this.$http = $http;
    this.appConfig = appConfig;
    // select options
    this.selected = {
      genero: null,
      localidad: null,
      edad: null
    };
    this.validation = {
      genero: null,
      localidad: null,
      edad: null
    };
    this.generosList = appConfig.generos;
    this.localidadesList = localidadesList;
    this.edadesList = _.range(appConfig.edades.desde, appConfig.edades.hasta);
  }
  
  $onInit() {
    if(this.encuesta.genero) {
      this.selected.genero = _.find(this.generosList, { value: this.encuesta.genero });
      this.validation.genero = 'valid';
    }
    let {departamento, ciudad} = this.encuesta;
    if (this.encuesta && departamento && ciudad) {
      this.selected.localidad = _.find(this.localidadesList, {departamento, ciudad});
      this.validation.localidad = 'valid';
    }
    if (this.encuesta && this.encuesta.edad) {
      this.selected.edad = this.encuesta.edad;
      this.validation.edad = 'valid';
    }
    this.setFinish();
  }

  selectOption(input, value) {
    this.validation[input] = 'waiting';
    this.$http.put(`/api/encuestas/${this.encuesta._id}`, { input, value })
      .then(result => {
        this.validation[input] = 'valid';
        this.setFinish();
      });
  }

  setFinish() {
    if (this.validation.genero === 'valid'
      && this.validation.localidad === 'valid'
      && this.validation.edad === 'valid'
    ) {
      this.validation.finish = true;
    }
  }
}

export default angular.module('acosoEnLineaApp.formulario', [])
  .component('formulario', {
    template: require('./formulario.html'),
    controller: formularioComponent,
    controllerAs: 'vm',
    bindings: {
      encuesta: '='
    }
  })
  .name;