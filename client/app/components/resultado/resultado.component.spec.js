'use strict';

describe('Component: resultado', function() {
  // load the component's module
  beforeEach(module('acosoEnLineaApp.resultado'));

  var resultadoComponent;

  // Initialize the component and a mock scope
  beforeEach(inject(function($componentController) {
    resultadoComponent = $componentController('resultado', {});
  }));

  it('should ...', function() {
    expect(1).toEqual(1);
  });
});
