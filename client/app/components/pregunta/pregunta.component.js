'use strict';
const angular = require('angular');
const _ = require('lodash');
const $ = require('jquery');
export class preguntaComponent {
  constructor($scope, $state, $http, appConfig, $timeout, $window) {
    'ngInject';
    this.$scope = $scope;
    this.$state = $state;
    this.$http = $http;
    this.$timeout = $timeout;
    this.$window = $window;
    this.index = 0;
    this.preguntas = appConfig.preguntas;
    this.result = {
      cat1: 0,
      cat2: 0,
      cat3: 0
    };
    this.setClasses();
    this.$window.localStorage.removeItem('respuestas');
    this.$window.localStorage.removeItem('resultInd');
    this.$window.localStorage.removeItem('encuestaId');
    this.$window.localStorage.removeItem('encuestaTimestamp');
  }

  setClasses() {
    let backgroundColors = ['success', 'info', 'warning', 'danger'];
    let lastBG;

    this.preguntas.forEach((pregunta, index) => {
      pregunta.classes = [];
      pregunta.classes.push(`pregunta-${pregunta.idPregunta}`);
      // par / impar
      if(index % 2 == 0) {
        pregunta.classes.push('par');
      }
      else {
        pregunta.classes.push('impar');
      }
      // asignar color diferente al anterior
      pregunta.classes.push( getColor() );
    });
    
    function getColor() {
      let currentBG = _.sample(backgroundColors);
      if(lastBG === currentBG) {
        return getColor();
      }
      lastBG = currentBG;
      return currentBG;
    }
  }

  goNext(respuesta) {
    if(this.animatingBtn) {
      return;
    }
    this.animateBtn(respuesta, () => {
      this.preguntas[this.index].respuesta = respuesta;
      if (this.index < (this.preguntas.length-1)) {
        this.index++;
      } else {
        this.calculoResultado();
        this.addEncuesta(result => {
          this.$window.localStorage.setItem('encuestaId', result.data._id);
          this.$window.localStorage.setItem('encuestaTimestamp', Date.now());
          this.$window.localStorage.setItem('resultInd', JSON.stringify(this.result));
          this.$window.localStorage.setItem('respuestas', JSON.stringify(this.preguntas));
          this.$state.go('main.resultado', {}, { reload: true });
        });
      }
    });
  }

  animateBtn(respuesta, cb) {
    let iconsToShow = 2;
    let animationDuration = 1500;
    this.iconSelected = this.getOneIcon(respuesta);
    this.animatingBtn = true;
    let $btn;
    if(respuesta === 'SI') {
      $btn = $('.btn-si');
    }
    else if(respuesta === 'NO') {
      $btn = $('.btn-no');
    }
    else if(respuesta === 'NOSE') {
      $btn = $('.btn-nose')
    }
    // shake
    $btn.addClass('shake');
    this.$timeout(() => {
      $btn.removeClass('shake');
      this.animatingBtn = false;
      cb();
    }, animationDuration);
  }

  getOneIcon(respuesta) {
    let newIcon;
    if(respuesta === 'SI') {
      newIcon = '#' + _.sample(['001-llanto', '002-triste', '004-enojado-1', '006-vomito', '009-enojado-3', '014-emoji-1']);
    }
    else if (respuesta === 'NO') {
      newIcon = '#' + _.sample(['007-enojado-2', '010-conmocionado', '012-conmocionado-1', '013-emoji', '002-triste']);
    }
    else if (respuesta === 'NOSE') {
      return '#012-conmocionado-1';
    }
    if(newIcon === this.iconSelected) {
      return this.getOneIcon(respuesta);
    }
    return newIcon;
  }

  calculoResultado() {
    // total de respuestas por categoría
    var tRespCat1 = 0;
    var tRespCat2 = 0;
    var tRespCat3 = 0;
    // total de respuestas positivas por categoría
    var tRespSiCat1 = 0;
    var tRespSiCat2 = 0;
    var tRespSiCat3 = 0;
    for (var i = 0; i < this.preguntas.length; i++) {
      //Acoso
      if (i == 0 || i == 1 || i == 4 || i == 5 || i == 6 || i == 7 || i == 8) {
        if (this.preguntas[i].respuesta == 'SI') {
          tRespSiCat1++;
        }
        if (this.preguntas[i].respuesta == 'SI' || this.preguntas[i].respuesta == 'NO') {
          tRespCat1++;
        }
      }
      //Discurso de odio
      if (i == 2 || i == 7) {
        if (this.preguntas[i].respuesta == 'SI') {
          tRespSiCat2++;
        }
        if (this.preguntas[i].respuesta == 'SI' || this.preguntas[i].respuesta == 'NO') {
          tRespCat2++;
        }
      }
      //Vigilancia
      if (i == 3 || i == 4 || i == 5) {
        if (this.preguntas[i].respuesta == 'SI') {
          tRespSiCat3++;
        }
        if (this.preguntas[i].respuesta == 'SI' || this.preguntas[i].respuesta == 'NO') {
          tRespCat3++;
        }
      }
    }
    this.result.tRespCat1 = tRespCat1;
    this.result.tRespCat2 = tRespCat2;
    this.result.tRespCat3 = tRespCat3;
    this.result.tRespSiCat1 = tRespSiCat1;
    this.result.tRespSiCat2 = tRespSiCat2;
    this.result.tRespSiCat3 = tRespSiCat3;
  }

  addEncuesta(cb) {
    this.$http.post('/api/encuestas', {
      preguntas: this.preguntas,
      result: this.result
    }).then(cb);
  }

  $onInit() {}
}

export default angular.module('acosoEnLineaApp.pregunta', [])
  .component('pregunta', {
    template: require('./pregunta.html'),
    controller: preguntaComponent,
    controllerAs: 'vm',
  })
  .name;
