'use strict';

describe('Component: MapaComponent', function() {
  // load the controller's module
  beforeEach(module('dataCuentaApp.mapa'));

  var mapa;

  // Initialize the controller and a mock scope
  beforeEach(inject(function($componentController) {
    mapa = $componentController('mapa', {});
  }));

  it('should ...', function() {
    expect(1).toEqual(1);
  });
});
