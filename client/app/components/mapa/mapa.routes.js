'use strict';

export default function($stateProvider) {
  'ngInject';
  $stateProvider
    .state('mapa', {
      url: '/mapa',
      template: '<mapa></mapa>'
    });
}
