'use strict';
const angular = require('angular');

const uiRouter = require('angular-ui-router');

import routes from './mapa.routes';

import * as L from 'leaflet';

import Chart from 'chart.js';


const $ = require('jquery');


export class mapa {
  /*@ngInject*/
  constructor(municipios, localidadesList, $window, $http, appConfig) {
    this.municipios = municipios;
    this.localidadesList = localidadesList;
    this.$window = $window;
    this.$http = $http;
    this.appConfig = appConfig;


    this.validation = {
      genero: null,
      localidad: null,
      edad: null
    };

  }



  $onInit() {
    //globales para los graficos
    Chart.defaults.global.defaultFontFamily = 'Lato';
    Chart.defaults.global.defaultFontSize = 12;
    Chart.defaults.global.defaultFontColor = '#777';

    var layerDistritos;
    let listaLocalidades = [];

    var exteriorStyle = {
      weight: 2,
      color: '#00AC9B',
      dashArray: '',
      fillOpacity: 0.1
    };

    var map = L.map('map').setView([-23.442503, -58.4438324], 7);
    L.tileLayer('https://api.mapbox.com/styles/v1/samsaurio/ck7urxj9606nr1isz39c1dxvz/tiles/256/{z}/{x}/{y}@2x?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery :copyright: <a href="https://www.mapbox.com/">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox/streets-v11',
      tileSize: 512,
      zoomOffset: -1,
      accessToken: 'pk.eyJ1Ijoic2Ftc2F1cmlvIiwiYSI6ImNqbjR1bDd6MTB1MGMzbG52Z2d5azNzMjQifQ.1EFe_Fl5OZ3A2EUik4dJ2Q'
    }).addTo(map);

    /*
      Agrega un estilo diferente a los distritos que son seleccionados en el mapa
    */
    function highlightFeature(e) {
      var layer = e.target;

      layer.setStyle({
        weight: 4,
        color: '#00AC9B',
        dashArray: '',
        fillOpacity: 0.7
      });

      if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
        layer.bringToFront();
      }
    }

    /*
    Elimina el estilo agregado al seleccionar un distrito
    */
    function resetHighlight(e) {
      layerDistritos.resetStyle(e.target);
    }

    /*
    Zoom al distrito cuando se selecciona.
    */
    function zoomToFeature(e) {
      var layer = e.target;
      map.fitBounds(e.target.getBounds());
      layer.openPopup();

    }

/*
  Funcion encargada de agregar la funcionalidad a cada capa que se va a agregar al mapa.
*/
    function onEachFeature(feature, layer) {
      listaLocalidades.push(feature.properties.dist_desc_) //lista de nombres de los distritos
      layer._leaflet_id = feature.properties.dist_desc_; //agrega a la capa generada el nombre por id para que su manejo sea mas facil

      layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: zoomToFeature
      });
    }


    layerDistritos = L.geoJson(this.municipios, {
      style: exteriorStyle,
      onEachFeature: onEachFeature
    }).addTo(map);

    map.fitBounds(layerDistritos.getBounds()); //centra mapa en paraguay luego de cargar geojson de distritos

    this.createDropdown(map, this.localidadesList);

    this.generateMarkers(map, listaLocalidades);

  } //termina el onit

  /*
    Funcion que crea el dropdown con un select de distritos
    -Recibe un instancia de un mapa creado previamente y una lista con los distritos encontrados dentro del archivo ciudades.js

  */
  createDropdown(map, listaLocalidades) {
    var departamentoAct;

    var dropdown = L.control({
      position: 'topright',
      title: 'Ciudades',
    });
    dropdown.onAdd = function(map) {
      var div = L.DomUtil.create('div', 'custom-select');
      var dropDownContent = '<select id="distritos">';

      for (var i = 0; i < listaLocalidades.length; i++) {

        var ciudad = listaLocalidades[i].ciudad.toUpperCase()
        ciudad.replace("'", "&#39;");

        if (i == 0) {
          dropDownContent += '<optgroup label=' + "'" + listaLocalidades[i].departamento + "'>";
          departamentoAct = listaLocalidades[i].departamento

        }
        if (listaLocalidades[i].departamento == departamentoAct) {

          dropDownContent += '<option value=' + '"' + ciudad + '">' + listaLocalidades[i].ciudad + '</option>';

        } else {
          departamentoAct = listaLocalidades[i].departamento
          dropDownContent += '<optgroup label=' + "'" + listaLocalidades[i].departamento + "'>";
          dropDownContent += '<option value=' + "'" + ciudad + "'>" + listaLocalidades[i].ciudad + '</option>';

        }
      }
      dropDownContent += '</select>';
      div.innerHTML = dropDownContent;
      div.firstChild.onmousedown = div.firstChild.ondblclick = L.DomEvent.stopPropagation;
      return div;
    };
    dropdown.addTo(map);

    $('select').change(function() {
      var distritoSelect = document.getElementById("distritos");
      var distritoSelected = distritoSelect.options[distritoSelect.selectedIndex].value;
      polySelect(map, distritoSelected);
    });

  };

  /*
  Genera los markers de las ciudades que tienen datos y agrega la funcionalidad a cada uno de ellos
  -Recibe un instancia de un mapa creado previamente y una lista con los distritos encontrados dentro del geoJson
  -Esta lista es generada desde oneachfeature, al momento de agregar las capas de cada distrito del geojson al mapa.
  -Cada elemento de esta lista debera mantenerse en mayusculas para su correcto funcionamiento
*/
  generateMarkers(map, listaLocalidades) {
    var markerStyle = L.icon({
      iconUrl: 'assets/images/localizacion.svg',
      iconSize: [38, 95], // size of the icon
      iconAnchor: [22, 94], // point of the icon which will correspond to marker's location
      popupAnchor: [-3, -76] // point from which the popup should open relative to the iconAnchor
    });

    this.$http.get('/api/encuestas').then(response => {
      for (var i = 0; i < listaLocalidades.length; i++) {
        var layer = map._layers[listaLocalidades[i]];
        var bounds = layer.getBounds();
        var center = bounds.getCenter();

        this.result = response.data;
        var porcentajes = getPorcentajes(this.result, listaLocalidades[i], "general", "masculino", [15, 32]);

        if (!(isNaN(porcentajes[0][0]) && isNaN(porcentajes[1][0]) && isNaN(porcentajes[2][0]))) {

          var ciudad= capitalizeWords(listaLocalidades[i].toLowerCase());
          var marker = new L.Marker(center, {
              icon: markerStyle,
              title: listaLocalidades[i]
            })  //inserta el html del grafico dentro del popup
            .bindPopup('<div id = "burbuja"><p class= tituloBurbuja> Resultados generales para: <br>' + ciudad + '</p><canvas id="burbujaChart"></canvas></div>')
            .on('popupopen', function(e) {
              this.result = response.data;
              var porcentajes = getPorcentajes(this.result, e.target.options.title, "general", "masculino", [15, 32]);
              var burbujaChart = document.getElementById("burbujaChart").getContext('2d');
              createBurbujaChart(burbujaChart, porcentajes)
              anadirdiv(this.result, e.target.options.title);

            })  //borra el codigo del contenido del sidebar y de la burbuja cuando se cierra el popup
            .on('popupclose', function(e) {
              var elem = document.getElementById("sidebar");

              while (elem.firstChild) {
                elem.removeChild(elem.firstChild);
              }
              var elemBurbuja = document.getElementById("burbuja");

              while (elemBurbuja.firstChild) {
                elemBurbuja.removeChild(elemBurbuja.firstChild);
              }

            }).addTo(map);
          console.log(marker);

        }
      }
    });

  }


} //aqui termina la clase


/*
Se obtiene los porcentajes de los generos y de los rangos de edad y se representan en los graficos
dentro  del sidebar
-como entrada es necesario la base y el nombre de la ciudad.
*/
function anadirdiv(base, distrito) {
  var porcentajeGeneroFemenino = getPorcentajes(base, distrito, "genero", "femenino", [15, 32]);

  var porcentajeGeneroMasculino = getPorcentajes(base, distrito, "genero", "masculino", [15, 32]);
  var porcentajeGeneroOtro = getPorcentajes(base, distrito, "genero", "otro", [15, 32]);
  var porcentajesGenero = [
    [porcentajeGeneroFemenino[0], porcentajeGeneroMasculino[0], porcentajeGeneroOtro[0]],
    [porcentajeGeneroFemenino[1], porcentajeGeneroMasculino[1], porcentajeGeneroOtro[1]],
    [porcentajeGeneroFemenino[2], porcentajeGeneroMasculino[2], porcentajeGeneroOtro[2]]
  ]

  var porcentajeEdadPrimerRango = getPorcentajes(base, distrito, "edad", "masculino", [12, 33]);
  var porcentajeEdadSegundoRango = getPorcentajes(base, distrito, "edad", "femenino", [34, 54]);
  var porcentajeEdadTercerRango = getPorcentajes(base, distrito, "edad", "otro", [55, 75]);
  var porcentajesEdad = [
    [porcentajeEdadPrimerRango[0], porcentajeEdadSegundoRango[0], porcentajeEdadTercerRango[0]],
    [porcentajeEdadPrimerRango[1], porcentajeEdadSegundoRango[1], porcentajeEdadTercerRango[1]],
    [porcentajeEdadPrimerRango[2], porcentajeEdadSegundoRango[2], porcentajeEdadTercerRango[2]]
  ]


  const div = document.createElement('sidebar');

  div.className = 'sidebar-wrapper';

  div.innerHTML = '  <div id="features" class="panel panel-default"><h3 class="panel-title">Resultados por género</h3>  <canvas id="genero"></canvas> <h3 class="panel-title">Resultados por edad</h3>  <canvas id="edad"></canvas> </div>';

  document.getElementById('sidebar').appendChild(div);

  var genero = document.getElementById('genero').getContext('2d');
  createChart("genero", genero, porcentajesGenero);

  var edad = document.getElementById('edad').getContext('2d');
  createChart("edad", edad, porcentajesEdad);
}

/*
crea el grafico que se mostrara en el canvas
el grafico puede ser de tipologia de genero o edad segun se especifique en el parametro "type"
*/
function createChart(type, chartName, porcentajes) {

  switch (type) {
    case 'edad':
      var listLabels = ['12-33 años', '34-54 años', '55-75 años'];
      break;
    case 'genero':
      var listLabels = ['Femenino', 'Masculino', 'Otro'];
      break;
  }
  var massPopChart = new Chart(chartName, {
    type: 'horizontalBar', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
    data: {
      labels: listLabels,
      datasets: [{
          label: 'Vigilancia',
          data: porcentajes[0],
          backgroundColor: ['rgb(255, 68, 106)', 'rgb(255, 68, 106)', 'rgb(255, 68, 106)']
        },
        {
          label: 'Acoso',
          data: porcentajes[1],
          backgroundColor: ['rgb(255, 246, 87)', 'rgb(255, 246, 87)', 'rgb(255, 246, 87)']
        },
        {
          label: 'Discurso de odio',
          data: porcentajes[2],
          backgroundColor: ['rgb(0, 176, 158)', 'rgb(0, 176, 158)', 'rgb(0, 176, 158)']
        }
      ]
    },
    options: {
      legend: {
        display: true,
        position: 'right',
        labels: {
          fontColor: '#000',
          boxWidth: 20,
        }
      },
      events: [],
      layout: {
        padding: {
          left: 25,
          right: 25,
          bottom: 25,
          top: 25
        }
      },
      scales: {
        xAxes: [{
          gridLines: {
            drawOnChartArea: false
          },
          ticks: {
            beginAtZero: true,
            steps: 10,
            max: 100
          }
        }],
        yAxes: [{
          gridLines: {
            drawOnChartArea: false
          }
        }]
      }
    }
  });
}

/*
  Crea el grafico de resultados generales dentro del PopUp de leaflet
*/

function createBurbujaChart(chartName, porcentajes) {
  var massPopChart = new Chart(chartName, {
    type: 'horizontalBar',
    data: {
      labels: [''],
      datasets: [{
          label: 'Vigilancia',
          data: porcentajes[0],
          backgroundColor: ['rgb(255, 68, 106)']
        },
        {
          label: 'Acoso',
          data: porcentajes[1],
          backgroundColor: ['rgb(255, 246, 87)']
        },
        {
          label: 'Discurso de odio',
          data: porcentajes[2],
          backgroundColor: ['rgb(0, 176, 158)']
        }
      ]
    },
    options: {
      legend: {
        display: true,
        position: 'top',
        labels: {
          boxWidth: 20,
          fontColor: '#000'
        }
      },
      events: [],
      scales: {
        xAxes: [{
          gridLines: {
            drawOnChartArea: false
          },
          ticks: {
            beginAtZero: true,
            steps: 10,
            max: 100
          }
        }],
        yAxes: [{
          gridLines: {
            drawOnChartArea: false
          }
        }]
      }
    }
  });
}


/*
.
como entrada es necesario, la base de datos, el nombre de la ciudad, el tipo(general, edad, genero), los rangos de edad(tupla) y genero(femenino,masculino, otro)
genera una media tomando en cuenta el tipo de infromacion que se desea obtener y a esa media saca el promedio de cada tipologia
*/
function getPorcentajes(resultados, ciudad, tipo, entradagen, entradaedad) {

  var sumaResPosAcoso = 0;
  var sumaRespNegAcoso = 0;
  var sumaResPosOdio = 0;
  var sumaRespNegOdio = 0;
  var sumaResPosVigilancia = 0;
  var sumaRespNegVigilancia = 0;
  var cantidadPersonas = 0;
  for (var i = 0; i < resultados.length; i++) {

    if (resultados[i].ciudad != null) {

      if (resultados[i].ciudad.toLowerCase() == ciudad.toLowerCase()) {

        if (tipo == "general" || (tipo == "genero" && entradagen == resultados[i].genero) || (tipo == "edad" && (entradaedad[0] <= resultados[i].edad && entradaedad[1] >= resultados[i].edad))) {

          cantidadPersonas++;
          sumaResPosAcoso = sumaResPosAcoso + resultados[i].result.tRespSiCat1
          sumaRespNegAcoso = sumaRespNegAcoso + resultados[i].result.tRespCat1
          sumaResPosOdio = sumaResPosOdio + resultados[i].result.tRespSiCat2
          sumaRespNegOdio = sumaRespNegOdio + resultados[i].result.tRespCat2

          sumaResPosVigilancia = sumaResPosVigilancia + resultados[i].result.tRespSiCat3
          sumaRespNegVigilancia = sumaRespNegVigilancia + resultados[i].result.tRespCat3

        }


      }

    }

  }

  var porcentajeAcoso = getPorcTipologia(cantidadPersonas, sumaResPosAcoso, sumaRespNegAcoso);
  var porcentajeOdio = getPorcTipologia(cantidadPersonas, sumaResPosOdio, sumaRespNegOdio);
  var porcentajeVigilancia = getPorcTipologia(cantidadPersonas, sumaResPosVigilancia, sumaRespNegVigilancia);

  if (tipo == "general") {
    var porcentajes = [
      [porcentajeAcoso],
      [porcentajeVigilancia],
      [porcentajeOdio]
    ];

    return porcentajes;
  } else {
    var porcentajes = [porcentajeAcoso, porcentajeVigilancia, porcentajeOdio];
    return porcentajes;
  }

}

/*
Obtiene el porcentaje de la media
porcentaje = respuesta positivas / respuestas negativas * 100
*/
function getPorcTipologia(total, resPos, resNeg) {
  var mediaPos = resPos / total;
  var mediaNeg = resNeg / total;

  var porcentaje = mediaPos / mediaNeg * 100;
  return porcentaje
}

/*
  Muestra el poligono de cada distrito
*/

function polySelect(map, distrito) {
  map._layers[distrito].fire('click');
  var layer = map._layers[distrito];
  map.fitBounds(layer.getBounds());
}

/*
coloca la primer letra de cada palabra en mayuscula
*/
function capitalizeWords(str){
  return str.split(" ").map(function(item){
           return item[0].toUpperCase() + item.substr(1,item.length);
       }).join(" ");
}


export default angular.module('dataCuentaApp.mapa', [uiRouter])
  .config(routes)
  .component('mapa', {
    template: require('./mapa.html'),
    controller: mapa,
    controllerAs: 'mp'
  })
  .name;
