'use strict';

import angular from 'angular';
import ngAnimate from 'angular-animate';
import ngCookies from 'angular-cookies';
import ngResource from 'angular-resource';
import ngSanitize from 'angular-sanitize';

import uiRouter from 'angular-ui-router';
import uiBootstrap from 'angular-ui-bootstrap';
import uiSelect from 'ui-select';

import { routeConfig } from './app.config';

import navbar from '../components/navbar/navbar.component';
import footer from '../components/footer/footer.component';
import main from './main/main.component';
import portada from './components/portada/portada.component';
import pregunta from './components/pregunta/pregunta.component';
import formulario from './components/formulario/formulario.component';
import resultado from './components/resultado/resultado.component';
import mapa from './components/mapa/mapa.component';

import constants from './app.constants';
import util from '../components/util/util.module';

import './app.less';

angular.module('acosoEnLineaApp', [ngAnimate, ngCookies, ngResource, ngSanitize, uiRouter, uiBootstrap, uiSelect, navbar,
  footer, main, constants, util, resultado, pregunta, formulario, portada, mapa
])
  .config(routeConfig);

angular.element(document)
  .ready(() => {
    angular.bootstrap(document, ['acosoEnLineaApp'], {
      strictDi: true
    });
  });
