'use strict';

export function routeConfig($urlRouterProvider, $locationProvider, uiSelectConfig) {
  'ngInject';

  $urlRouterProvider.otherwise('/');

  $locationProvider.html5Mode(true);

  uiSelectConfig.theme = 'bootstrap';
}
