# data.cuenta

Esta es una plataforma interactiva que pretende visibilizar, medir y conocer las experiencias de violencia que han sido mediadas por la tecnología. Desde la selfie controladora de tus sueños hasta esos mensajitos indeseados de gente indeseable. Está disponible en: https://datacuenta.tedic.org/

Queremos saber qué te ha pasado, y queremos brindarte consejos y soluciones para hacerle frente a las formas de violencia que muchas veces no reconocemos en nuestro cotidiano.

Para más información accede a: https://www.tedic.org/proyecto/data-cuenta/

# Desarrollo

Esta herramienta ha sido desarrollada en conjunto con [WebLab](https://www.weblab.com.py).

This project was generated with the [Angular Full-Stack Generator](https://github.com/DaftMonk/generator-angular-fullstack) version 4.2.3.

## Getting Started

### Prerequisites

- [Git](https://git-scm.com/)
- [Node.js and npm](nodejs.org) Node >= 4.x.x, npm >= 2.x.x
- [Gulp](http://gulpjs.com/) (`npm install --global gulp`)
- [MongoDB](https://www.mongodb.org/) - Keep a running daemon with `mongod`

### Developing

1. Run `npm install` to install server dependencies.

2. Run `mongod` in a separate shell to keep an instance of the MongoDB Daemon running

3. Run `gulp serve` to start the development server. It should automatically open the client in your browser when ready.

## Build & development

Run `gulp build` for building and `gulp serve` for preview.

## Testing

Running `npm test` will run the unit tests with karma.

## With docker

**Dependences:** You need `docker` and `docker-compose` installed

Build and up (first time):
`docker-compose up -d`

If you change something, then build, down and up again:
`docker-compose build && docker-compose down && docker-compose up -d`

**Config file:** check `server/config/environment/development.js` and change db connection from `mongodb://localhost/acosoenlinea-dev` to `mongodb://db/acosoenlinea-dev`.

### Production

Para hacer correr en modo producción, son necesarios los siguientes cambios
 - `docker-compose.yml`: reemplazar puertos `3000` por `8080`
 - `Dockerfile`: reemplazar `gulp serve` por `gulp serve:dist`

## Respaldos
Hay dos procesos para obtener un respaldo de la base de datos:

**A.** Obtener un archivo CSV con los resultados

**B.** Obtener un dump de la BBDD

Se desarrolla cada uno.

### A. Para obtener CSV
**1.** Hacer el respaldo CSV, corriendo esto dentro de la carpeta del proyecto:

`docker-compose exec app npm run csv-prod`

Obs: Los respaldos estarán en la carpeta exported-data. Si se sigue el docker-compose de este proyecto, ya estará disponible en la carpeta del proyecto.

### B. Para obtener dump

**1.** Hacer el dump de la BBDD, corriendo esto dentro de la carpeta del proyecto:

`docker-compose exec db mongodump --gzip --db acosoenlinea`

**2.** Copiarlo a la carpeta del proyecto

`docker cp CONTENEDOR:/dump .`

reeemplazando CONTENEDOR por el nombre del contenedor de la BBDD.

Obs: Los respaldos están en la carpeta **dump**.

**3.** Si fuera necesario **restaurar**, hay que hacer:

```
docker cp dump CONTENEDOR:/.
docker-compose exec db mongorestore --gzip -d acosoenlinea dump/acosoenlinea
```
